﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int numeroDigitado;
                int i = 1;
                Console.Write("Digite um número: ");
                numeroDigitado = int.Parse(Console.ReadLine());

                do
                {
                    Console.WriteLine(numeroDigitado + " x " + i + " = " + (numeroDigitado * i));
                    i++;
                }
                while (i <= 10);
            }
            catch (Exception error)
            {
                Console.WriteLine("Ocorreu um erro no programa pois você deve digitar um número! Erro: " + error.Message);
            }
            finally
            {
                Console.WriteLine("Aperte qualquer tecla...");
                Console.ReadKey();
            }
        }
    }
}
